/* On submit of message form,validation is here
mailFormat and nameFormat is regex for checking email validation
and name to contain alphabets only
*/
onContactFormSubmit = () => {
  let name = document.getElementById("name").value;
  let email = document.getElementById("email").value;
  let message = document.getElementById("message").value;
  const mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  const nameFormat = /^[a-zA-Z]+$/;
  //   Checks if name,message is empty or not and sets error message
  let error = 0;
  if (name == "") {
    document.getElementById("name-err-message").innerHTML = errorMessage(
      "Name"
    );
    error = 1;
  }
  if (name != "" && name.length < 3) {
    document.getElementById("name-err-message").innerHTML =
      "Name must be of atleast 3 characters.";
    error = 1;
  }
  if (name != "" && !name.match(nameFormat)) {
    document.getElementById("name-err-message").innerHTML =
      "Name can only contain alphabets.";
    error = 1;
  }
  if (email == "") {
    document.getElementById("email-err-message").innerHTML = errorMessage(
      "Email"
    );
    error = 1;
  }
  if (email != "" && !email.match(mailformat)) {
    document.getElementById("email-err-message").innerHTML =
      "Enter a valid Email please.";
    error = 1;
  }
  if (message == "") {
    document.getElementById("message-err-message").innerHTML = errorMessage(
      "Message"
    );
    error = 1;
  }
  if (error == 0) {
    alert(
      "Thank you, " + name + " for sending me message.I'll get to you soon!"
    );
    // Resetting form value to empty
    document.getElementById("name").value = "";
    document.getElementById("email").value = "";
    document.getElementById("message").value = "";
  }
};
// End of validation contact form

// Empty Error message .. This is a common method,displays common message
function errorMessage(fieldName) {
  return fieldName + " field is required.";
}

// This method will remove error message if input field is filled.this function accepts string argument.
// if name is given as arguments,then it clears name error,if email,then remove email error.or else remove message error
function onFieldChange(value) {
  if (value == "name") {
    document.getElementById("name-err-message").innerHTML = null;
  }
  if (value == "email") {
    document.getElementById("email-err-message").innerHTML = null;
  }
  if (value == "message") {
    document.getElementById("message-err-message").innerHTML = null;
  }
}

/* Toggle Section 
For toggling sections. toggleSection Method is called on clicked event listener is made, and it takes argument
sectionName. And depending upon sectionName,doToggleMainMethod() is called which takes three arguments 
sectionName(id),section button id and message to show on button, and this method does main toggling process.*/
function toggleSection(sectionName) {
  switch (sectionName) {
    case "skills":
      doToggleMainMethod("skills", "skill-toggle-button", "Skills");
      break;
    case "contact":
      doToggleMainMethod("contact", "contact-toggle-button", "Contact");
      break;
    case "education":
      doToggleMainMethod("education", "education-toggle-button", "Education");
      break;
    case "experience":
      doToggleMainMethod(
        "experience",
        "experience-toggle-button",
        "Experience"
      );
      break;
    case "projects":
      doToggleMainMethod("projects", "projects-toggle-button", "Projects");
      break;
    default:
      console.log("Error");
      break;
  }
}

/* Main togglinf process is done in this method if display is block then,it makes none and viceversa */
function doToggleMainMethod(sectionName, buttonId, buttonMessage) {
  const sectionSelector = document.getElementById(sectionName);
  const buttonSelector = document.getElementById(buttonId);
  if (sectionSelector.style.display === "none") {
    sectionSelector.style.display = "block";
    buttonSelector.innerHTML = "Hide " + buttonMessage;
    buttonSelector.style.backgroundColor = "red";
  } else {
    sectionSelector.style.display = "none";
    buttonSelector.innerHTML = "Show " + buttonMessage;
    buttonSelector.style.backgroundColor = "green";
  }
}
/* END OF TOGGLE SECTION */
