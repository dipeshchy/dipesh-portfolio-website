# Dipesh Portfolio

## About

This is my portfolio website developed with **HTML** and **CSS** and **Javascript**. You can get information about me from this portfolio website. It contains information about my previous qualifications,my working experience,programming skills that I have and the projects that I have done.

## Technologies Used

I have used **HTML**, **CSS** and **Javascript** for this project. I used Visual Studio Code as text-editor. **Git** has been for version control and BitBucket as
remote git repository. I used **wireframe** technology for drawing the layout. We can use various online tools and applications to
draw the layout of the application, but I used simple paper and pencil to draw the layout for this project. HTML(Hyper Text Markup Language) has been used to git layout and structure to the website. CSS(Cascading Style Sheet) has been used to give style and make the website more attractive. JavaScript
has been used to make the website interactive. Javascript is used here for form validation.

## Description

This is a simple portfolio website describing about me. There are mainly six sections in this website excluding footer and header.
Header contains a logo and navbar. Navbar includes menu containing about,skills,experience,projects done,education and contact
linking to corresponding content in the website. About me section contains information about me like my introduction,address,email,phone and
also contains my photo. Skills section contains different programming skills that I know.Experience section includes description of
my previous work experience.And the next section contains projects done by me.Education section contains details of my previous
educational qualifications. Anyone visiting this website can contact me by sending message through contact me section. And lastly, Footer consist of
copyright declaration.
I have also used some images in this website.Images used are logo,my photo and social media's icons which can be found in img folder.

#### I have used following HTML tags for this project :

- div
- section
- nav
- ul
- li
- a
- img
- h1
- h2
- h3
- h4
- blockquote
- span
- strong
- form
- label
- input
- textarea
- table
- tr
- td
- footer

#### I have used following HTML elements for this project :

- href
- class
- id
- input
- src
- alt
- value
- max

### I have used following styles

* margin
* font-size
* background-color
* position
* padding
* text-decoration
* list-style-type
* border
* box shadow
* hover

### Javascript Methods

- onContactFormSubmit()
  This method includes validation and pop up alert message on submission of form.
  Validation includes non-empty field,text field validation and email validation.

- errorMessage()
  This method is a common method,which returns the error message based on arguments.It accepts an argument for field like name,email and message.

- onfieldchange(inputField)
  This method removes error message when that input field is filled.

- toggleSection(sectionName)
  This method is used to toggle the respective section whether to show or hide the section. This method calls
  doToggleMainMethod() where the main task of toggling is done.

## Contributing

Pull requests are welcome.
Please make sure to update tests as appropriate
